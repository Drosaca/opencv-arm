# Install script for directory: /home/drosaca/opencv/samples/python

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RELEASE")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xsamplesx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/opencv4/samples/python" TYPE FILE PERMISSIONS OWNER_READ GROUP_READ WORLD_READ FILES
    "/home/drosaca/opencv/samples/python/_coverage.py"
    "/home/drosaca/opencv/samples/python/_doc.py"
    "/home/drosaca/opencv/samples/python/asift.py"
    "/home/drosaca/opencv/samples/python/audio_spectrogram.py"
    "/home/drosaca/opencv/samples/python/browse.py"
    "/home/drosaca/opencv/samples/python/calibrate.py"
    "/home/drosaca/opencv/samples/python/camera_calibration_show_extrinsics.py"
    "/home/drosaca/opencv/samples/python/camshift.py"
    "/home/drosaca/opencv/samples/python/coherence.py"
    "/home/drosaca/opencv/samples/python/color_histogram.py"
    "/home/drosaca/opencv/samples/python/common.py"
    "/home/drosaca/opencv/samples/python/contours.py"
    "/home/drosaca/opencv/samples/python/deconvolution.py"
    "/home/drosaca/opencv/samples/python/demo.py"
    "/home/drosaca/opencv/samples/python/dft.py"
    "/home/drosaca/opencv/samples/python/digits.py"
    "/home/drosaca/opencv/samples/python/digits_adjust.py"
    "/home/drosaca/opencv/samples/python/digits_video.py"
    "/home/drosaca/opencv/samples/python/dis_opt_flow.py"
    "/home/drosaca/opencv/samples/python/distrans.py"
    "/home/drosaca/opencv/samples/python/drawing.py"
    "/home/drosaca/opencv/samples/python/edge.py"
    "/home/drosaca/opencv/samples/python/essential_mat_reconstr.py"
    "/home/drosaca/opencv/samples/python/facedetect.py"
    "/home/drosaca/opencv/samples/python/feature_homography.py"
    "/home/drosaca/opencv/samples/python/find_obj.py"
    "/home/drosaca/opencv/samples/python/fitline.py"
    "/home/drosaca/opencv/samples/python/floodfill.py"
    "/home/drosaca/opencv/samples/python/gabor_threads.py"
    "/home/drosaca/opencv/samples/python/gaussian_mix.py"
    "/home/drosaca/opencv/samples/python/grabcut.py"
    "/home/drosaca/opencv/samples/python/hist.py"
    "/home/drosaca/opencv/samples/python/houghcircles.py"
    "/home/drosaca/opencv/samples/python/houghlines.py"
    "/home/drosaca/opencv/samples/python/inpaint.py"
    "/home/drosaca/opencv/samples/python/kalman.py"
    "/home/drosaca/opencv/samples/python/kmeans.py"
    "/home/drosaca/opencv/samples/python/laplace.py"
    "/home/drosaca/opencv/samples/python/lappyr.py"
    "/home/drosaca/opencv/samples/python/letter_recog.py"
    "/home/drosaca/opencv/samples/python/lk_homography.py"
    "/home/drosaca/opencv/samples/python/lk_track.py"
    "/home/drosaca/opencv/samples/python/logpolar.py"
    "/home/drosaca/opencv/samples/python/morphology.py"
    "/home/drosaca/opencv/samples/python/mosse.py"
    "/home/drosaca/opencv/samples/python/mouse_and_match.py"
    "/home/drosaca/opencv/samples/python/mser.py"
    "/home/drosaca/opencv/samples/python/opencv_version.py"
    "/home/drosaca/opencv/samples/python/opt_flow.py"
    "/home/drosaca/opencv/samples/python/peopledetect.py"
    "/home/drosaca/opencv/samples/python/plane_ar.py"
    "/home/drosaca/opencv/samples/python/plane_tracker.py"
    "/home/drosaca/opencv/samples/python/qrcode.py"
    "/home/drosaca/opencv/samples/python/squares.py"
    "/home/drosaca/opencv/samples/python/stereo_match.py"
    "/home/drosaca/opencv/samples/python/stitching.py"
    "/home/drosaca/opencv/samples/python/stitching_detailed.py"
    "/home/drosaca/opencv/samples/python/text_skewness_correction.py"
    "/home/drosaca/opencv/samples/python/texture_flow.py"
    "/home/drosaca/opencv/samples/python/tracker.py"
    "/home/drosaca/opencv/samples/python/tst_scene_render.py"
    "/home/drosaca/opencv/samples/python/turing.py"
    "/home/drosaca/opencv/samples/python/video.py"
    "/home/drosaca/opencv/samples/python/video_threaded.py"
    "/home/drosaca/opencv/samples/python/video_v4l2.py"
    "/home/drosaca/opencv/samples/python/watershed.py"
    )
endif()

