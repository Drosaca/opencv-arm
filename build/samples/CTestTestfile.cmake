# CMake generated Testfile for 
# Source directory: /home/drosaca/opencv/samples
# Build directory: /home/drosaca/opencv/build/samples
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("cpp")
subdirs("java/tutorial_code")
subdirs("dnn")
subdirs("gpu")
subdirs("tapi")
subdirs("opencl")
subdirs("sycl")
subdirs("python")
