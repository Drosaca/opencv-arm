
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/drosaca/opencv/samples/cpp/tutorial_code/snippets/core_reduce.cpp" "samples/cpp/CMakeFiles/example_snippet_core_reduce.dir/tutorial_code/snippets/core_reduce.cpp.o" "gcc" "samples/cpp/CMakeFiles/example_snippet_core_reduce.dir/tutorial_code/snippets/core_reduce.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/drosaca/opencv/build/3rdparty/ippiw/CMakeFiles/ippiw.dir/DependInfo.cmake"
  "/home/drosaca/opencv/build/modules/gapi/CMakeFiles/opencv_gapi.dir/DependInfo.cmake"
  "/home/drosaca/opencv/build/modules/highgui/CMakeFiles/opencv_highgui.dir/DependInfo.cmake"
  "/home/drosaca/opencv/build/modules/ml/CMakeFiles/opencv_ml.dir/DependInfo.cmake"
  "/home/drosaca/opencv/build/modules/objdetect/CMakeFiles/opencv_objdetect.dir/DependInfo.cmake"
  "/home/drosaca/opencv/build/modules/photo/CMakeFiles/opencv_photo.dir/DependInfo.cmake"
  "/home/drosaca/opencv/build/modules/stitching/CMakeFiles/opencv_stitching.dir/DependInfo.cmake"
  "/home/drosaca/opencv/build/modules/video/CMakeFiles/opencv_video.dir/DependInfo.cmake"
  "/home/drosaca/opencv/build/modules/videoio/CMakeFiles/opencv_videoio.dir/DependInfo.cmake"
  "/home/drosaca/opencv/build/modules/imgcodecs/CMakeFiles/opencv_imgcodecs.dir/DependInfo.cmake"
  "/home/drosaca/opencv/build/modules/calib3d/CMakeFiles/opencv_calib3d.dir/DependInfo.cmake"
  "/home/drosaca/opencv/build/modules/features2d/CMakeFiles/opencv_features2d.dir/DependInfo.cmake"
  "/home/drosaca/opencv/build/modules/flann/CMakeFiles/opencv_flann.dir/DependInfo.cmake"
  "/home/drosaca/opencv/build/modules/dnn/CMakeFiles/opencv_dnn.dir/DependInfo.cmake"
  "/home/drosaca/opencv/build/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  "/home/drosaca/opencv/build/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
