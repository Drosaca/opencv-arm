# CMake generated Testfile for 
# Source directory: /home/drosaca/opencv/apps
# Build directory: /home/drosaca/opencv/build/apps
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("annotation")
subdirs("visualisation")
subdirs("interactive-calibration")
subdirs("version")
subdirs("model-diagnostics")
